from luminos.utils import version, constants, standarddir
from luminos.core.Bridge import BridgeObject, Bridge, Variant
from PyQt5.QtWidgets import QApplication

versionInstance = None
pluginManagerInstance = None
directoryInstance = None


class Version(BridgeObject):
    noop_signal = Bridge.signal()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @Bridge.property(str, notify=noop_signal)
    def CHROMIUM(self):
        return version._chromium_version()

    @Bridge.property(str, notify=noop_signal)
    def QT(self):
        return constants.qt_version()


class PluginManager(BridgeObject):
    noop_signal = Bridge.signal()
    pluginAdded = Bridge.signal(str)
    pluginRemoved = Bridge.signal(str)
    pluginActivated = Bridge.signal(str)
    pluginDeactivated = Bridge.signal(str)
    loadedPlugins = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        app = QApplication.instance()
        self._pluginManager = app.pluginManager
        self._pluginManager.pluginActivated.connect(lambda name: self.pluginActivated.emit(name))
        self._pluginManager.pluginDeactivated.connect(lambda name: self.pluginDeactivated.emit(name))
        self._pluginManager.pluginAdded.connect(self._onPluginAdded)
        self._pluginManager.pluginRemoved.connect(self._onPluginRemoved)

    def _onPluginAdded(self, name):
        self.pluginAdded.emit(name)

    def _onPluginRemoved(self, name):
        self.pluginRemoved.emit(name)

    @Bridge.property(Variant)
    def loadedPlugins(self):
        return [key for key in self._pluginManager._loadedPlugins.keys()]

    @Bridge.method(str)
    def addPluginPath(self, path: str):
        self._pluginManager.addPluginPath(path)

    @Bridge.method(str)
    def enablePlugin(self, name: str):
        self._pluginManager.enablePlugin(name)

    @Bridge.method(str)
    def disablePlugin(self, name: str):
        self._pluginManager.disablePlugin(name)


class Directory(BridgeObject):
    noop_signal = Bridge.signal()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @Bridge.property(str, notify=noop_signal)
    def cache(self):
        return standarddir.cache()

    @Bridge.property(str, notify=noop_signal)
    def data(self):
        return standarddir.data()

    @Bridge.property(str, notify=noop_signal)
    def config(self):
        return standarddir.config()


def beforeLoad(channel, page):
    global versionInstance, pluginManagerInstance, directoryInstance

    versionInstance = Version()
    pluginManagerInstance = PluginManager()
    directoryInstance = Directory()

    channel.registerObject("Directory", directoryInstance)
    channel.registerObject("PluginManager", pluginManagerInstance)
    channel.registerObject("Version", versionInstance)


def deactivate():
    global versionInstance, pluginManagerInstance, directoryInstance
    versionInstance = None
    pluginManagerInstance = None
    directoryInstance = None
