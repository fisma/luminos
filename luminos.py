#!/usr/bin/env python3

"""Simple launcher for luminos."""
import luminos.luminos as luminos
import sys

if __name__ == "__main__":
    sys.exit(luminos.main())
