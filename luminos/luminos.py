import sys
from luminos.utils import utils


def main():
    parser = utils.get_argparser()
    argv = sys.argv[1:]
    args = parser.parse_args(argv)

    from luminos.utils import earlyinit
    earlyinit.early_init(args)

    import luminos.Application as app
    sys.exit(app.run(args))
