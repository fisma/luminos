
from typing import TypeVar

from PyQt5.QtCore import Qt, QUrl
from PyQt5.QtWidgets import QVBoxLayout, QSplitter, QApplication
from PyQt5.QtWebChannel import QWebChannel

from luminos.browser.webengine import WebView
from luminos.core.AbstractWindow import AbstractWindow
from luminos.utils import log, usertypes

Url = TypeVar('Url', str, QUrl)


class Window(AbstractWindow):
    app = None
    bridgeInitialized = False
    _pendingLoad = False

    def __init__(self, app):
        super().__init__(app)
        self.channel = QWebChannel()
        self.splitter = QSplitter(self)
        self.splitter.setOrientation(Qt.Vertical)
        self.layout = QVBoxLayout(self)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.addWidget(self.splitter)

        self.webview = WebView.LWebView(private=None, parent=self.splitter)
        self.webview.toggleDevTools.connect(self.toggleInspector)
        self.webview.loadChanged.connect(self._loadChanged)

        self.splitter.addWidget(self.webview)

    def _loadChanged(self, e):
        app = self.app
        if app is None:
            app = QApplication.instance()

        if e == usertypes.LoadEvent.BEFORE_LOAD and not self.bridgeInitialized:
            self._initBridge()
            self.bridgeInitialized = True

        from luminos.Application import Application
        # app is probably None in test
        if app is not None and isinstance(app, Application):
            page = self.webview.page()
            if e == usertypes.LoadEvent.BEFORE_LOAD:
                app.pluginManager.beforeLoad.emit(self.channel, page)
            elif e == usertypes.LoadEvent.STARTED:
                app.pluginManager.loadStarted.emit(page)
            elif e == usertypes.LoadEvent.FINISHED:
                app.pluginManager.loadFinished.emit(page)

    def _initBridge(self):
        log.webview.debug("Initializing web channel bridge...")
        page = self.webview.page()

        page.injectScript(":/qtwebchannel/qwebchannel.js", "QWebChannel API")
        page.injectScript(":/luminos/js/bridge.js", "Luminos Bridge")

        app = self.app
        if app is None:
            app = QApplication.instance()

        from luminos.Application import Application
        # app is probably None in test
        if app is not None and isinstance(app, Application):
            app.pluginManager.bridgeInitialize.emit(page)

        page.setWebChannel(self.channel)

    def loadScript(self, path, name):
        """Inject javascript to be loaded when document is created"""
        page = self.webview.page()
        script = self._createWebengineScript(path, name)
        page.scripts().insert(script)
