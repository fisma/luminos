"""A browser based application framework."""

import os.path

__author__ = "M Sayuti"
__copyright__ = "Copyright 2019 M Sayuti"
__license__ = "GPL"
__maintainer__ = __author__
__email__ = "ekoputrapratama27@yahoo.com"
__version_info__ = (1, 0, 0)
__version__ = ".".join(str(e) for e in __version_info__)
__description__ = "A browser based application framework."

basedir = os.path.dirname(os.path.realpath(__file__))
