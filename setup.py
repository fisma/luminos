# Always prefer setuptools over distutils
import re
import os
import ast
from setuptools import setup
from scripts import setupcommon as common

try:
    BASEDIR = os.path.dirname(os.path.realpath(__file__))
except NameError:
    BASEDIR = None


def read_file(name):
    """Get the string contained in the file named name."""
    with common.open_file(name, 'r', encoding='utf-8') as f:
        return f.read()


def _get_constant(name):
    """Read a __magic__ constant from qutebrowser/__init__.py.

    We don't import qutebrowser here because it can go wrong for multiple
    reasons. Instead we use re/ast to get the value directly from the source
    file.

    Args:
        name: The name of the argument to get.

    Return:
        The value of the argument.
    """
    field_re = re.compile(r'__{}__\s+=\s+(.*)'.format(re.escape(name)))
    path = os.path.join(BASEDIR, 'luminos', '__init__.py')
    line = field_re.search(read_file(path)).group(1)
    value = ast.literal_eval(line)
    return value


setup(
    name="luminos",
    version="0.3.2",
    packages=[
        "luminos",
        "luminos.base",
        "luminos.widgets",
        "luminos.windows",
        "luminos.utils",
        "luminos.browser",
    ],
    url="https://gitlab.com/fisma/luminos",
    license="MIT",
    author="Fisma Linux Project",
    author_email="dustin@antergos.com",
    description="Desktop application SDK for creating Universal Linux Applications.",
    install_requires=["PyQt5", "ruamel.yaml<0.15"],
    package_data={"": ["whither.yml"]},
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Environment :: X11 Applications",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License (MIT)",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: JavaScript",
        "Programming Language :: Python :: 3.4",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Topic :: Software Development",
        "Topic :: Software Development :: Libraries :: Application Frameworks",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "Topic :: Software Development :: User Interfaces",
    ],
    keywords="desktop-application-sdk framework sdk javascript universal html5",
)
